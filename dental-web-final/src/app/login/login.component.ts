import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../shared/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})


export class LoginComponent implements OnInit {
  formModel = {
    UserName:'',
    Password: ''
  }
  constructor(private service: UserService,
    private router: Router,
    private toastr: ToastrService) { }

  ngOnInit(): void {
    if(localStorage.getItem('token') !== null){
      this.router.navigateByUrl("/client-menu");
    }
  }

  OnSubmit(form: NgForm): void {
    this.service.login(form.value)
      .subscribe(
        (res:any) => {
          localStorage.setItem("token", res.token);
          this.router.navigateByUrl("/client-menu");
        },
        err =>{
          if(err.status == 400){
            this.toastr.error('Incorrect username or password', 'Authentication Failed');
          }
          else {
            console.log(err);
          }
        }
      );
  }

}
