import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-client-menu',
  templateUrl: './client-menu.component.html',
  styleUrls: ['./client-menu.component.css']
})
export class ClientMenuComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  onLogout(){
    localStorage.removeItem('token');
    this.router.navigate(["/user/login"]);
  }

  selectLanguage(lang: string){
    lang === 'en' ? localStorage.setItem('isEnglish', 'true') : localStorage.setItem('isEnglish', 'false');
  }

  getLanguage(): string{
    return localStorage.getItem('isEnglish') + "";
  }

}
