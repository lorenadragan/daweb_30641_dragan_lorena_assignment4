import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { Doctor } from '../doctor';
import { Medic } from '../medic';
import { Serviciu_DTO } from '../serviciu-dto';
import { User } from '../user';
@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private fb: FormBuilder,
    private http: HttpClient) { }

  readonly BaseURI = "http://localhost:62264/api";
  formModel = this.fb.group({
    UserName: ['', Validators.required],
    Email: ['', Validators.email],
    FullName: [''],
    Passwords: this.fb.group({
      Password: ['', [Validators.required, Validators.minLength(4)]],
      ConfirmPassword: ['',  [Validators.required, Validators.minLength(4)]]
    }, {validator: this.comparePasswords }),
    Role: ['', Validators.required]
  });

  comparePasswords(fb: FormGroup): void {
    let confirmPswrdCtrl = fb.get('ConfirmPassword');
    //PasswordMismatch
    //confirmPswrdCtrl.erros = {passwordMismatch:true}
    if(confirmPswrdCtrl.errors == null || 'passwordMismatch' in confirmPswrdCtrl.errors){
      if(fb.get('Password').value != confirmPswrdCtrl.value){
        confirmPswrdCtrl.setErrors({passwordMismatch:true});
      }
      else {
        confirmPswrdCtrl.setErrors(null);
      }
    }
  }

  register(): Observable<any> {
    var body = {
      UserName: this.formModel.value.UserName,
      Email: this.formModel.value.Email,
      Password: this.formModel.value.Passwords.Password,
      FullName: this.formModel.value.FullName,
      Role: this.formModel.value.Role
    };

    return this.http.post<any>( this.BaseURI + "/ApplicationUser/Register", body)
  }

  login(formData): Observable<any> {
    return this.http.post<any>( this.BaseURI + "/ApplicationUser/Login", formData)
  }

  roleMatch(allowedRoles): boolean{
    var isMatch = false;
    var payLoad = JSON.parse(window.atob(localStorage.getItem('token').split('.')[1]));
    var userRole = payLoad.role;
    allowedRoles.forEach(element => {
      if(userRole == element){
        isMatch = true;
        return false;
      }
    });
    return isMatch;
  }

  getUserProfile(){
    // var tokenHeader = new HttpHeaders({"Authorization":'Bearer ' + localStorage.getItem('token')})
    return this.http.get<User>(this.BaseURI + "/UserProfile").toPromise();
  }

  getMedicProfile(){
    return this.http.get<Medic>(this.BaseURI + "/UserProfile/Medic");
  }

  getMedicalServices(){
    return this.http.get<Serviciu_DTO[]>(this.BaseURI + "/Services").toPromise();
  }

  async AddServiceToClient(serviciuId: number, clientId: number){
    var serviciu = {
      clientId: clientId,
      serviciuId: serviciuId
    }
    return this.http.post(this.BaseURI + "/UserServices", serviciu).toPromise();
  }

  async DeleteServiceFromClient(serviciuId: string){
    console.log(serviciuId);
    return this.http.delete(this.BaseURI + "/UserServices/" + serviciuId).toPromise();
  }

  async GetMedics(){
    return this.http.get<Doctor[]>(this.BaseURI + "/Medics").toPromise();
  }

  async CreateAppointment(el, date, clientId){
    var programare = {
      medicId:el["id"],
      date:date,
      clientId: clientId
    };
    return this.http.post(this.BaseURI + "/programari", programare).toPromise();
  }

  async DeleteAppointmentService(appointmentId: string){
    return this.http.delete(this.BaseURI + "/programari/" + appointmentId).toPromise();
  }

  GetProgramariForCurrentMedic(medicId: number){
    return this.http.get(this.BaseURI + "/Programari/medicId=" + medicId);
  }
}
