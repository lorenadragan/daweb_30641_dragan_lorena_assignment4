import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AboutUsComponent } from './about-us/about-us.component';
import { AuthGuard } from './auth/auth.guard';
import { ClientMenuComponent } from './client-menu/client-menu.component';
import { ContactComponent } from './contact/contact.component';
import { CreateAppointmentComponent } from './create-appointment/create-appointment.component';
import { ForbiddenComponent } from './forbidden/forbidden.component';
import { HomePageComponent } from './home-page/home-page.component';
import { LoginComponent } from './login/login.component';
import { MedicCalendarComponent } from './medic-calendar/medic-calendar.component';
import { MedicStatisticaComponent } from './medic-statistica/medic-statistica.component';
import { NewsPageComponent } from './news-page/news-page.component';
import { ProfilePageComponent } from './profile-page/profile-page.component';
import { RegistrationComponent } from './registration/registration.component';
import { ServicesComponent } from './services/services.component';
import { TeamComponent } from './team/team.component';
import { UserComponent } from './user/user.component';


const routes: Routes = [
  { path: "", redirectTo:"/user/login", pathMatch: "full"},
  { path: "user", component:UserComponent,
  children: [
    { path: "registration", component: RegistrationComponent}, // /user/register
    { path: "login", component: LoginComponent} // /user/register
  ]},
  {path: "client-menu", component: ClientMenuComponent, canActivate:[AuthGuard],
  children: [
    { path: "", redirectTo: "home", pathMatch: "full"},
    { path: 'home', component: HomePageComponent},
    { path: 'despre-noi', component: AboutUsComponent},
    { path: 'medici', component: TeamComponent},
    { path: 'servicii-tarife', component: ServicesComponent},
    { path: 'contact', component: ContactComponent, canActivate:[AuthGuard], data: {permittedRoles:['Client']}},
    { path: 'profile', component: ProfilePageComponent,canActivate:[AuthGuard], data: {permittedRoles:['Client']}},
    { path: 'create-appointment', component: CreateAppointmentComponent,canActivate:[AuthGuard], data: {permittedRoles:['Client']}},
    { path: 'noutati', component: NewsPageComponent},
    { path: 'medic-calendar', component: MedicCalendarComponent, canActivate:[AuthGuard], data: {permittedRoles:['Medic']}},
    { path: 'medic-statistica', component: MedicStatisticaComponent, canActivate:[AuthGuard], data: {permittedRoles:['Medic']}}
  ]},
  { path: "forbidden", component: ForbiddenComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
