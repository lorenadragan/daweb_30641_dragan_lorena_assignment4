import { Component, OnInit } from '@angular/core';
import { isThisISOWeek } from 'date-fns';
import * as moment from 'moment';
import { DaysMonths } from '../days-months';
import { UserService } from '../shared/user.service';
@Component({
  selector: 'app-medic-statistica',
  templateUrl: './medic-statistica.component.html',
  styleUrls: ['./medic-statistica.component.css']
})
export class MedicStatisticaComponent implements OnInit {

  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  user;
  public mbarChartLabels: string[] = [];
  public barChartType: string = 'bar';
  public barChartLegend: boolean = true;
  public barChartColors: Array<any> = [
    // {
    //   backgroundColor: 'rgba(105,159,177,0.2)',
    //   borderColor: 'rgba(105,159,177,1)',
    //   pointBackgroundColor: 'rgba(105,159,177,1)',
    //   pointBorderColor: '#fafafa',
    //   pointHoverBackgroundColor: '#fafafa',
    //   pointHoverBorderColor: 'rgba(105,159,177)'
    // },
    {
      backgroundColor: 'rgba(77,20,96,0.3)',
      borderColor: 'rgba(77,20,96,1)',
      pointBackgroundColor: 'rgba(77,20,96,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,20,96,1)'
    }

  ];

  public barChartData: any[] = [
    { data: [], label: '' }
  ];
  public daysInCurrentMonth;
  public daysInCurrentMonthForRequest = [];
  public nrProgramariForEachDayInCurrentMonth: Array<number>;
  public monthName: string;
  public monthToNrProgramari: Map<string, number> = new Map<string, number>();
  constructor(private service: UserService) { }

  ngOnInit(): void {

    //get month name to display above the graphic
    this.monthName = moment().format("MMMM");
    //get days of current month  to display on graphic(format = Saturday, May 1st 2021)
    this.daysInCurrentMonth = DaysMonths.getDaysInMonth(moment().month(), moment().year());

    //initializare dictionar pereche data-nr programari
    this.daysInCurrentMonth.forEach(day => {
      this.monthToNrProgramari.set(moment(day).format("YYYY-MM-DD"), 0);
    });

    //set labels for chart
    this.daysInCurrentMonth.forEach(day => {
      this.mbarChartLabels.push(moment(day).format("dddd, MMMM Do YYYY"));
    });


  this.service.getMedicProfile().subscribe((data) => {
      this.service.GetProgramariForCurrentMedic(data["medicId"]).subscribe((programari:any) => {
        programari.forEach(programareMedic => {
          var dayFormatted = moment(programareMedic["date"]).format("YYYY-MM-DD");
          var nrProgramari = this.monthToNrProgramari.get(dayFormatted);
          nrProgramari++;
          this.monthToNrProgramari.set(dayFormatted, nrProgramari);
        });
        console.log(this.monthToNrProgramari)

        var arrayWithProgramari: number[] = [];
        arrayWithProgramari.length = this.daysInCurrentMonth.length;
        for(var i=0; i<arrayWithProgramari.length; i++){
          arrayWithProgramari[i] = 0;
        }
        var index = 0;
        this.monthToNrProgramari.forEach(element => {
          arrayWithProgramari[index]=element;
          index++;
        });

        //set data in barChart
        // var obj = {data:arrayWithProgramari, label: "Programari"};
        this.barChartData[0]["data"] = arrayWithProgramari;
        this.barChartData[0]["label"] = "Programari"
      });});
  }


  getLanguage(): string{
    return localStorage.getItem('isEnglish') + "";
  }


  public chartClicked(e: any): void {
    console.log(e);
  }
}
