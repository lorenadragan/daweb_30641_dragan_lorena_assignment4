import { AfterViewInit, Component, OnInit } from '@angular/core';
import { CalendarEvent, CalendarView } from 'angular-calendar';
import { colors } from '../../app/colors';
import { isSameDay, isSameMonth } from 'date-fns';
import { User } from '../user';
import { UserService } from '../shared/user.service';
import { Medic } from '../medic';
import { Appointment_DTO } from '../appointment-dto';
@Component({
  selector: 'app-medic-calendar',
  templateUrl: './medic-calendar.component.html',
  styleUrls: ['./medic-calendar.component.css']
})
export class MedicCalendarComponent implements OnInit{
  viewDate: Date = new Date();
  view: CalendarView = CalendarView.Month;
  user;
  programari;

  events: CalendarEvent[];

  constructor(private service: UserService) {

  }

  ngOnInit() {
    this.service.getMedicProfile().subscribe((data) => {
      this.service.GetProgramariForCurrentMedic(data["medicId"]).subscribe((data:any) => {
        var internalEvents: CalendarEvent[] = [];
        for(var i=0; i<data.length; i++){
          var event: CalendarEvent = {
            start :new Date(data[i]["date"]),
            title: data[i]["clientName"],
            color: colors.red
          };
          internalEvents.push(event);
          this.events = internalEvents;
        }
      })
    })
  }

  getLanguage(): string{
    return localStorage.getItem('isEnglish') + "";
  }

  closeOpenMonthViewDay() {
    this.activeDayIsOpen = false;
  }
  activeDayIsOpen: boolean;

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
        this.viewDate = date;
      }
    }
  }
}
