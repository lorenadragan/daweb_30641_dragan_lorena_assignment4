export class Medic {
  email: string;
  medicId: number;
  name: string;
  userId: string;
  userName: string;

  public constructor(email?: string, medicId?: number, name?: string, userId?: string, userName?: string){
    this.email = email;
    this.medicId = medicId;
    this.name = name;
    this.userId = userId;
    this.userName = userName;
  }
}
