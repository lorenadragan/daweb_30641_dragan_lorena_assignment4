import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.css']
})
export class TeamComponent implements OnInit {
  person3: boolean;
  person4: boolean;
  person5: boolean;
  person9: boolean;
  person10: boolean;
  person11: boolean;
  person12: boolean;
  person14: boolean;
  constructor() { }

  ngOnInit(): void {
    this.person3 = false;
    this.person4 = false;
    this.person5 = false;
    this.person9 = false;
    this.person10 = false;
    this.person11 = false;
    this.person12 = false;
    this.person14 = false;
  }

  displayInfo(personalNumber: number){
    switch(personalNumber){
      case 3:
        this.person3 = true;
        this.person4 = false;
        this.person5 = false;
        this.person9 = false;
        this.person10 = false;
        this.person11 = false;
        this.person12 = false;
        this.person14 = false;
        break;
      case 4:
        this.person3 = false;
        this.person4 = true;
        this.person5 = false;
        this.person9 = false;
        this.person10 = false;
        this.person11 = false;
        this.person12 = false;
        this.person14 = false;
        break;
      case 5:
        this.person3 = false;
        this.person4 = false;
        this.person5 = true;
        this.person9 = false;
        this.person10 = false;
        this.person11 = false;
        this.person12 = false;
        this.person14 = false;
        break;
      case 9:
        this.person3 = false;
        this.person4 = false;
        this.person5 = false;
        this.person9 = true;
        this.person10 = false;
        this.person11 = false;
        this.person12 = false;
        this.person14 = false;
        break;
      case 10:
        this.person3 = false;
        this.person4 = false;
        this.person5 = false;
        this.person9 = false;
        this.person10 = true;
        this.person11 = false;
        this.person12 = false;
        this.person14 = false;
        break;
      case 11:
        this.person3 = false;
        this.person4 = false;
        this.person5 = false;
        this.person9 = false;
        this.person10 = false;
        this.person11 = true;
        this.person12 = false;
        this.person14 = false;
        break;
      case 12:
        this.person3 = false;
        this.person4 = false;
        this.person5 = false;
        this.person9 = false;
        this.person10 = false;
        this.person11 = false;
        this.person12 = true;
        this.person14 = false;
        break;
      case 14:
        this.person3 = false;
        this.person4 = false;
        this.person5 = false;
        this.person9 = false;
        this.person10 = false;
        this.person11 = false;
        this.person12 = false;
        this.person14 = true;
        break;
      default:
        this.person3 = false;
        this.person4 = false;
        this.person5 = false;
        this.person9 = false;
        this.person10 = false;
        this.person11 = false;
        this.person12 = false;
        this.person14 = false;
        break;
    }
  }
  getLanguage(): string{
    return localStorage.getItem('isEnglish') + "";
  }


}
