import { Appointment_DTO } from "./appointment-dto";
import { Serviciu_DTO } from "./serviciu-dto";

export class User {
  clientId: number;
  email: string;
  fullName: string;
  userName: string;
  serviciiVizate: Serviciu_DTO[] = [];
  programari: Appointment_DTO[] = [];

  constructor(clientId?: number, email?: string, fullName?: string, userName?: string, serviciiVizate?: Serviciu_DTO[], programari?){
    this.clientId = clientId;
    this.email = email;
    this.fullName = fullName;
    this.userName = userName;
    this.serviciiVizate = serviciiVizate;
    this.programari = programari;
  }
}
