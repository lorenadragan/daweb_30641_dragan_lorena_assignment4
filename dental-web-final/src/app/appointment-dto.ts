export class Appointment_DTO {
  id: number;
  client_id: number;
  date: string;
  doctor_name: string;

  public constructor(id?: number, client_id?: number, date?: string, doctor_name?: string){
    this.id = id;
    this.client_id = client_id;
    this.doctor_name = doctor_name;
    this.date = date;
  }
}
