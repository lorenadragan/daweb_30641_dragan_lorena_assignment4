import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Email } from '../email';
@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
  form = new FormGroup({
    name: new FormControl('', [Validators.required, Validators.minLength(3)]),
    body: new FormControl('', Validators.required)
  });
  constructor(private _http: HttpClient) { }

  ngOnInit(): void {
  }
  getLanguage(): string{
    return localStorage.getItem('isEnglish') + "";
  }

  get f(){
    return this.form.controls;
  }

  submit(){
    if(this.form.status === 'VALID'){
      let emailToSend = new Email();
      emailToSend.body = this.form.value["body"];
      emailToSend.name = this.form.value["name"];

      this._http.post<any>('http://localhost:3000/mail', {name: emailToSend.name, body: emailToSend.body})
        .subscribe(data => {
          console.log(data);
        },
        (err: HttpErrorResponse) => {
          console.log(err);
        })

    }
  }

  resetValue(){
    this.form.reset({name: '', email: '', body: ''});
  }

}
