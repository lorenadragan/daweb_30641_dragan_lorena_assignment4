import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../shared/user.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  constructor(public service: UserService,
    private toastr: ToastrService) { }

  ngOnInit(): void {
    this.service.formModel.reset();
  }

  OnSubmit(){
    this.service.register()
      .subscribe(
        (res: any) => {
          if(res.succeeded){
            //reset the controls
            this.service.formModel.reset();
            this.toastr.success('New User Created', 'Registration Successfull');
          } else {
            res.errors.forEach(element => {
              switch (element.code) {
                case 'DuplicateUserName':
                  console.log("Username is already taken");
                  this.toastr.error(element.description, "Registration failed");
                  break;

                default:
                  console.log("Registration failed");
                  break;
              }
            });
          }
        },
        err => {
          console.log(err);
        }
      )
  }

}
