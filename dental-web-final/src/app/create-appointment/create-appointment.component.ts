import { Component, OnInit } from '@angular/core';
import { Appointment_DTO } from '../appointment-dto';
import { Doctor } from '../doctor';
import { UserService } from '../shared/user.service';
import { User } from '../user';

@Component({
  selector: 'app-create-appointment',
  templateUrl: './create-appointment.component.html',
  styleUrls: ['./create-appointment.component.css']
})
export class CreateAppointmentComponent implements OnInit {
  user: User = new User();
  constructor(private service:UserService) { }
  headElements = ["Id Programare", "Doctor Name", "Date"];
  doctorsHeadElements = ["Id", "Doctor Name"]
  doctors: Doctor[] = [];
  programariClienti:Appointment_DTO[] = [];

  async ngOnInit(){
    this.user = await this.service.getUserProfile();
    this.doctors = await this.service.GetMedics();
  }

  getLanguage(): string{
    return localStorage.getItem('isEnglish') + "";
  }

  async CreateAppointment(el){
    let date = (<HTMLInputElement>document.getElementById('date')).value;
    await this.service.CreateAppointment(el, date, this.user["clientId"]);
    this.user = await this.service.getUserProfile();
  }

  async DeleteAppointment(el){
    await this.service.DeleteAppointmentService(el["id"])
    this.user = await this.service.getUserProfile();
  }

  async CreateAppointmentEngleza(el){
    let date = (<HTMLInputElement>document.getElementById('date_en')).value;

    await this.service.CreateAppointment(el, date, this.user["clientId"]);
    this.user = await this.service.getUserProfile();
  }
}
