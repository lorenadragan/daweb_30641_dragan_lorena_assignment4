import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Serviciu_DTO } from '../serviciu-dto';
import { UserService } from '../shared/user.service';
import { User } from '../user';

@Component({
  selector: 'app-profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.css']
})
export class ProfilePageComponent implements OnInit {
  readonly BaseURI = "http://localhost:62264/api";
  user: User = new User();
  headElements = ["Id", "Name", "Price"];
  file: any;
  dbServices: Serviciu_DTO[] = [];

  constructor(private service: UserService) { }

  async ngOnInit() {
    this.user = await this.service.getUserProfile();
    this.dbServices = await this.service.getMedicalServices();
  }

  getLanguage(): string {
    return localStorage.getItem('isEnglish') + "";
  }

  async AdaugaServiciu(el: Serviciu_DTO){
    console.log(el["id"])
    await this.service.AddServiceToClient(el["id"], this.user.clientId);
    this.user = await this.service.getUserProfile();
  }

  UpdatePersonalInformation(){

  }

  UpdatePersonalInformationEngleza(){

  }

  async StergeServiciuEngleza(){
    var serviciu_id = (<HTMLInputElement>document.getElementById('serviciu_id_en')).value;

    await this.service.DeleteServiceFromClient(serviciu_id);
    this.user = await this.service.getUserProfile();
  }

  async StergeServiciu(){
    var serviciu_id = (<HTMLInputElement>document.getElementById('serviciu_id')).value;

    await this.service.DeleteServiceFromClient(serviciu_id);
    this.user = await this.service.getUserProfile();
  }

  changeFile(file) {
    return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result);
        reader.onerror = error => reject(error);
    });
  }

  uploadFile(event) {
    console.log(event)
      if (event.target.value) {
        this.file = event.target.files[0];
        const type = this.file.type;
        this.changeFile(this.file).then((base64: string): any => {
            // console.log(base64);
            // this.fileBlob = this.b64Blob([base64], type);
            // console.log(this.fileBlob)
            sessionStorage.setItem('user_gdpr', this.file);
        });
    } else alert('Nothing')
  }

}
